from z3 import *
import networkx as nx
import heapq
import sys
import time
from itertools import combinations
from statistics import *


####################################################read file####################################################################
#Prend en entrée le nom du fichier et retroune un graphe coloré
def readFile(filename):
    g = nx.Graph()
    with open(filename) as f:
        for line in f:
            data = [str(x) for x in line.split()] 
            if data[0] == "v":
                g.add_node(int(data[1]),type=int(data[2]),candidate=False,chapeau=False)
            if data[0] == "e":
                g.add_edge(int(data[1]),int(data[2]),hetero=False)

    return g


##################################################setHetHom#######################################################################
#prend en entrée un graphe bicoloré et retourne:
#le graphe avec les attributs "candidate" sur les sommets (on ne s'en sert pas pour le moment)
#le graphe avec les attributs "homo" ou "hetero" sur les arêtes (on ne s'en sert pas non plus)
#l'ensemble des arêtes hétéros (on s'en sert, important)
#l'ensemble des arêtes homo (idem)
def setHetHom(g):
	het=set()
	hom=set()
	for e in g.edges():
		u=e[0]
		v=e[1]
		if g.nodes[u]['type']!=g.nodes[v]['type']:
			g.edges[e]['hetero']=True
			het.add(e)
			g.nodes[u]['candidate']=True
			g.nodes[v]['candidate']=True
		else:
			hom.add(e)
	return g, het, hom


##########################################################réduction du graphe####################################################
#prend en entrée un graphe, un ensemble d'arêtes homos et hétéros et retourne:
#le graphe réduit
#l'ensemble des arêtes hétéros (qui normalement ne change pas)
#l'esemble des arêtes homos (qui normalement change beaucoup)
#la liste des composantes réellement connexes (idem)
def reduce_graph(h,ht,hm):
	g=h.copy()
	g.remove_edges_from(ht)
	ccc=nx.connected_components(g)
	liste_cc=[]
	for cc in ccc:
		liste_cc.append(cc)

	g.remove_edges_from(hm)
	hm.clear()
	for cc in liste_cc:
		pas_candid=[]
		candid=[]
		for u in cc:
			if g.nodes[u]['candidate']==False:
				pas_candid.append(u)
			else:
				candid.append(u)

		g.remove_nodes_from(cc)
		g.add_path(candid)
		for e in g.edges():
			hm.add(e)
	ccc=nx.connected_components(g)		
	liste_cc=[]
	for cc in ccc:
		liste_cc.append(list(cc))
	g.add_edges_from(ht)
	return g, ht, hm, liste_cc



#####################################################recalcul des CC##################################################################
#prend en entrée un graphe, ensemble hétéros et ensemble homos et retourne la nouvelle liste des cc. On ne s'en sert pas ici (c'est pour le greedy)
def recompute_cc(g, ht, hm):
	g.remove_edges_from(ht)
	ccc=nx.connected_components(g)
	ll_cc=[]
	for cc in ccc:
		ll_cc.append(cc)
	g.add_edges_from(ht)
	return 	ll_cc


############################################################ création des variables x_u################################################
#prend en entrée un graphe et un entier (numéro de la NFV) et retourne les variables x_u_i ainsi qu'un dico pour retourver l'index de x_u_i
def create_x_variables(g,k):
    var_x=[]
    dict_x=dict()
    cpt=0
    for u in g.nodes():
        for t in range(1,k+1):
            dict_x[cpt]=[u,t]
            var_x.append(Bool("x_%i_%i" % (u,t)))
            cpt+=1
    return var_x, dict_x




#############################################################création des variables p_j_j'##############################################"
#prend en entrée un graphe et la liste des cc et retourne les variables p_j_j' ainsi qu'un dico pour retourver l'index de p_j_j'
def create_p_variables(g, liste_ccs):
    var_p=[]
    dict_p=dict()
    cpt=0
    for jj in liste_ccs:                                                    #j'
        for j in [j for j in liste_ccs if liste_ccs.index(j)!=0 and jj!=j]:
            dict_p[cpt]=[liste_ccs.index(j),liste_ccs.index(jj)]
            var_p.append(Bool("p_%i_%i" % (liste_ccs.index(j), liste_ccs.index(jj))))
            cpt+=1
    return var_p, dict_p



#############################################################création des variables l_j_i#############################################
#end en entrée le graphe et la liste des cc et renvoie les variables l_j_i (niveau) et un dico pour retrouver l'index de l_j_i
def create_l_variables(g, liste_ccs):
    var_l=[]
    dict_l=dict()
    cpt=0
    for j in liste_ccs:
        for t in range(0,len(liste_ccs)):
            dict_l[cpt]=[liste_ccs.index(j),t]
            var_l.append(Bool("l_%i_%i" % (liste_ccs.index(j),t)))
            cpt+=1
    return var_l, dict_l




###############################################################création de toutes les variables#####################################
#prend en entrée le graphe, la liste des cc et le nombre de chapeaux, et crée toutes les variables ainsi que les dicos de leur index
def create_all_variables(g,liste_ccs,k):
    var_x, dict_x=create_x_variables(g,k)
    var_p, dict_p=create_p_variables(g,liste_ccs)
    var_l, dict_l=create_l_variables(g,liste_ccs)
    return var_x, var_p, var_l, dict_x, dict_p, dict_l




#################################################################récupère la variable p_j_j'#####################################
# prend entrée dict_p et la liste des variables var_p, deux num de cc j et jj renvoie la variable correspondant à p_j_jj (jj=j')
def get_var_p(var_p, dict_p, j,jj):
    for cpt in dict_p.keys():
        if dict_p[cpt]==[j,jj]:
            return var_p[cpt]




##################################################################récupère la variable x_u#########################################
# prend entrée dict_l et la liste des variables val_l, un num de cc j et un niveau l et renvoie la variable correspondant à var_l_j_l
def get_var_x(var_x, dict_x, u,i):
    #print("je cherche", u, i)
    for cpt in dict_x.keys():
        if dict_x[cpt]==[u,i]:
            return var_x[cpt]




################################################################récupère la variable l_j_i#######################################"
# prend entrée dict_l et la liste des variables val_l, un num de cc j et un niveau i et renvoie la variable correspondant à var_l_j_i
def get_var_l(var_l, dict_l, j,i):
    for cpt in dict_l.keys():
        if dict_l[cpt]==[j,i]:
            return var_l[cpt]




################################################################crée la partie sur les niveaux de la formule "parent"##############
### cette formule crée OR_{1\leq k\leq cc-1} (l_j,i AND l_j',i-1), permet de savoir si un j'(noté jj) peut être père de j selon leur niveaux
def level_relation(j,jj,var_l, dict_l, liste_ccs):
    formula=[]
    for i in range(1,len(liste_ccs)):
        formula.append(And([get_var_l(var_l,dict_l,j,i),get_var_l(var_l,dict_l,jj,i-1)]))
    formula=Or(formula)
    return formula





########################################################## crée la formule sur les x_u dans la relation "parent"#####################
### cette formule crée OR_{1 \leq i\leq k} (x_u,i OU x_v,i) # ici k doit être nb_chap
def cover_relation(var_x, dict_x, u, v, k):
    formula=[]
    for i in range(1,k+1):
        formula.append(Or([get_var_x(var_x,dict_x,u,i),get_var_x(var_x,dict_x,v,i)]))
    formula=Or(formula)
    return formula



########################################################### vérifie si j et j' sont bien reliées par un lien hétéro###################
### cette fonction prend en entrée le graphe, les cc j et jj, la liste des cc, et retourne vrai si il existe un lien hétéro entre j et jj (j')
def relation_parent_possible(g, j, jj, liste_ccs):
    for u in liste_ccs[j]:
        cpt=0
        for v in [v for v in g.neighbors(u) if v in liste_ccs[jj]]:
            cpt+=1
    if cpt>0:
        return True
    else:
        return False


########################################################### crée toute la formule "parent" pour j et j' données########################
### formule parent (avec AND au lieu de Implies)
def parent_relation(g, var_x, dict_x, var_p, dict_p,var_l, dict_l, j, jj, liste_ccs, k):
    outer_or=[]
    for u in liste_ccs[j]:    #(le ET sur tous le sommets de j)
        inter_or=[]
        for v in [v for v in g.neighbors(u) if v in liste_ccs[jj]]:  #le ET sur tous les sommets de j'
            inter_or.append(cover_relation(var_x, dict_x, u, v, k))
        if len(inter_or)!=0:
            inter_or=Or(inter_or) #les OR sur (u in j, v in j', arête uv couverte, i.e., x_u OU x_v)
            outer_or.append(inter_or)
    formula_right=Or(outer_or)

    formula_right=And([formula_right,level_relation(j,jj,var_l, dict_l,liste_ccs)]) 
    total_parent_formula=And(formula_right,get_var_p(var_p,dict_p, j,jj))    #le AND qui remplace le =>
   
    return total_parent_formula



########################################################### UNIQUE ############################################################
#cree la formule UNIQUE sur les variables en entrée. On fait entre sorte qu'elle ne soit pas appelée si "variables" est vide
def unique(variables):
    if len(variables)==1:
        return variables[0]
    formula_at_least=Or(variables)
    formula_unique=And([AtMostOne(variables),formula_at_least])
    return formula_unique


######################################################### AtMostOne #############################################################
#cree la formule AT_most one, on fait en sorte de ne pas l'appeler si len(variables)<= 1
def AtMostOne(variables):
    if len(variables)<=1:
        return variables
    formula_at_most=[]
    for comb in combinations(variables, 2):
        formula_at_most.append(Or([Not(comb[0]), Not(comb[1])]))
    formula_at_most=And(formula_at_most)
    return formula_at_most



########################################################## l'algo principal qui crée la formule SAT #################################
#prend en enrée le graphes, les arêtes homos et hétéros, la liste des cc et k (le nombre de chapeaux)
#renvoie 
def sat2(h,Ht,Hm, liste_cc,nb_chap):
    t_deb=time.time()   #temps de début de l'algo SAT
    var_x, var_p, var_l, dict_x, dict_p, dict_l=create_all_variables(h,liste_cc,nb_chap)   #création des variables
    outer_for_all_graph=[]
    for j in [j for j in liste_cc if liste_cc.index(j)!=0]:    #le forall j!=0 cc
        inter_for_j=[]
        for jj in [jj for jj in liste_cc if j!=jj]:             # pour tout j' parent possible de j
            inter_for_j.append(parent_relation(h, var_x, dict_x, var_p, dict_p, var_l, dict_l, liste_cc.index(j), liste_cc.index(jj), liste_cc, nb_chap))    #creation de la relation "parent"
        inter_for_j=Or(inter_for_j)                        #le OR sur les j' sur la relation "parent"
        outer_for_all_graph.append(inter_for_j)
    outer_for_all_graph=And(outer_for_all_graph)           # le AND sur les j pour qu'ils aient tous un parent
    level_root=get_var_l(var_l, dict_l,0,0)       #ajout de la contraine que la première cc est au niveau 0
    
    liste_uniques=[]     
    for u in h.nodes():  #chaque u peut avoir au plus un chapeau
        liste_x_u=[]
        for i in range(1,nb_chap+1):
            liste_x_u.append(get_var_x(var_x, dict_x,u,i))
        if len(liste_x_u)>1:
            liste_uniques.append(AtMostOne(liste_x_u))


    for i in range(1,nb_chap+1):         # chaque chapeau est sur un seul sommet
        liste_x_i=[]
        for u in h.nodes():
            liste_x_i.append(get_var_x(var_x, dict_x,u,i))
        if len(liste_x_i)>=1:
            liste_uniques.append(unique(liste_x_i))
    
    
    for j in [j for j in liste_cc if liste_cc.index(j)!=0]:   #chaque j sauf la racine a exactement un parent
        liste_one_father=[]
        for jj in [jj for jj in liste_cc if jj!=j]:
            liste_one_father.append(get_var_p(var_p, dict_p,liste_cc.index(j),liste_cc.index(jj)))
        if len(liste_one_father)>=1:
            liste_uniques.append(unique(liste_one_father))

    for j in liste_cc:   ######################### chaque sommet a un seul niveau
        liste_one_level=[]
        for l in range(0,len(liste_cc)):
            liste_one_level.append(get_var_l(var_l,dict_l,liste_cc.index(j),l))
        if len(liste_one_level)>=1:
            liste_uniques.append(unique(liste_one_level))


    liste_uniques=And(liste_uniques) 				   #and sur toutes les règles d'unicité
    tout=And([liste_uniques, outer_for_all_graph,level_root])		# création de la formule générale
    s=Solver()							#intiation du solveur
    s.add(tout)						  #affectation de la formule générale au solveur


    t_ecriture=time.time()-t_deb
    result=str(s.check())					#appel au solver (SAT ou UNSAT)
    t_resolve=time.time()-t_deb-t_ecriture
    return str(s.check()), t_ecriture, t_resolve
    


################################################### main #########################################################
liste_result=[]
liste_temps=[]
liste_temps=[]
for iiii in range(1,100):
    chemin=sys.argv[1]+"test-"+str(iiii)+".out"    #1er argument est le dossier où se trouve les instances du graphe (ex., taille_33_BICS/)
    t=time.time()
    h=readFile(chemin)    #lecture du fichire test-iiii.out
    h, Ht, Hm =setHetHom(h)     #calcul hétéros et homos
    h,Ht,Hm, liste_cc =reduce_graph(h,Ht,Hm)      #réduction du graphe
    nb=1

    t_ecr=0
    t_res=0
    if len(Ht)==0:     ###############cas avec 0 chapeaux #########################################
       nb=0
       liste_result.append(nb)
       liste_temps.append(time.time()-t)
       print("Graphe test-"+str(iiii)+".out"+"   nb chapeaux = 0, temps écriture =",time.time()-t, "temps résolution= 0")
	
    else:
        while True:
            res,t1,t2=sat2(h,Ht, Hm,liste_cc,nb)
            t_ecr+=t1
            t_res+=t2
            if res=="sat":
                break
            nb+=1
        print("Graphe test-"+str(iiii)+".out"+"   nb chapeaux =",nb , "  temps écriture =",t_ecr, "temps résolution =", t_res)    
        liste_result.append(nb)
        liste_temps.append(time.time()-t)
print("moyenne chapeaux ="+str(mean(liste_result))+"   Moyenne temps total = "+str(mean(liste_temps))+"   max chapeau ="+str(max(liste_result))+"  max temps ="+str(max(liste_temps))+"\n")


exit()
