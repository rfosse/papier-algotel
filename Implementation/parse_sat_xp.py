import sys
import glob
import os
import numpy as np


sommets = set()
def readfile(filename):
    global sommets
    seconds = []
    chapeaux = []
    resol = []
    with open(filename) as f:
        for line in f:
            data = [str(x) for x in line.split()]
            if "résolution:" in data:
                resol.append(float(data[3]))
            if "secondes" in data:
                seconds.append(float(data[2]))
            if "ont" in data:
                chapeaux.append(int(data[0]))

    return seconds,resol,chapeaux

def complete_l(l):
    for ll in l:
        while(len(ll) < 40):
            ll.append(-1)
            
def mean_ll(l):
    res = []
    for i in range(len(l[0])):
        m = 0
        cpt = 0
        tmp = []
        for ll in l:
            if ll[i] != -1:
                tmp.append(ll[i])
                cpt += 1
        res.append(np.median(tmp))
    return res



def main():
    dir = sys.argv[1]
    list_sec_total = []
    list_chapeaux_total = []
    list_resol_total = []
    list_crea_total = []

    for file in os.listdir(dir):
        s,r,c = readfile(dir+"/"+file)
        list_crea_total.append(r)
        list_sec_total.append(s)
        list_chapeaux_total.append(c)


    l = []
    complete_l(list_sec_total)
    complete_l(list_chapeaux_total)
    complete_l(list_crea_total)

    m_c = mean_ll(list_chapeaux_total)
    m_s = mean_ll(list_sec_total)
    m_crea = mean_ll(list_crea_total)

    print("moyenne des chapeaux par taille:")
    for i in m_c:
        print(i)

    print("moyenne des secondes par taille:")
    for i in m_s:
        print(i)
    
    print("moyenne des resolutions par taille:")
    for i in m_crea:
        print(i)
    return 0

if __name__ == "__main__":
    main()