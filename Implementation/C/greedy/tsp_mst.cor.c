//
// TSP_MST - SOLUTION
//

// Crée un graphe à n sommets et sans arêtes. Les listes (potentielles
// de voisins) sont de tailles n, mais attention les degrés ne sont
// pas initialisés ! Il faudra le faire impérativement dans tsp_mst().
graph createGraph(int n) {
  graph G;
  G.n = n;
  G.deg = malloc(n * sizeof(*(G.deg)));
  G.list = malloc(n * sizeof(*(G.list)));
  for (int u = 0; u < n; u++)
    G.list[u] = malloc(n * sizeof(int)); // taille n par défaut
  return G;
}

// Libère un graphe G et ses listes.
void freeGraph(graph G) {
  for (int u = 0; u < G.n; u++)
    free(G.list[u]);
  free(G.list);
  free(G.deg);
}

// Ajoute l'arête u-v au graphe G de manière symétrique. Les degrés de
// u et v doivent être à jour et les listes suffisamment grandes.
void addEdge(graph G, int u, int v) {
  G.list[u][G.deg[u]++] = v;
  G.list[v][G.deg[v]++] = u;
}

// Une arête u-v avec un poids.
typedef struct {
  int u, v;      // extrémités de l'arête u-v
  double weight; // poids de l'arête u-v
} edge;

// Fonction de comparaison du poids de deux arêtes à utiliser avec
// qsort() pour un tri par ordre croissant. Ne pas hésiter à utiliser
// "man qsort" en ligne de commande pour l'aide sur cette fonction de
// la libraire standard du C.
int compEdge(const void *e1, const void *e2) {
  double const x = ((edge*)e1)->weight;
  double const y = ((edge*)e2)->weight;
  return (x > y) - (x < y); // -1,0,+1 suivant que x<y, x=y, ou x>y
}

// Fusionne les arbres de racines x et y selon l'heuristique du rank
void Union(int x, int y, int *parent, int *rank) {
  if (rank[x] > rank[y])
    parent[y] = x;
  else {
    parent[x] = y;
    if (rank[x] == rank[y])
      rank[y]++;
  }
}

// Renvoie la racine de l'arbre contenant u selon l'heuristique de la
// compression de chemin.
int Find(int u, int *parent) {
  if (u != parent[u])
    parent[u] = Find(parent[u], parent);
  return parent[u];
}

// Calcule dans le tableau Q l'ordre de première visite des sommets du
// graphe G selon un parcours en profondeur d'abord à partir du sommet
// u. Le paramètre p est le sommet parent de u. On doit avoir p<0 si u
// est l'origine du parcours (premier appel).
void dfs(graph G, int u, int *Q, int p) {
  static int t; // t = temps ou indice (variable globale) du tableau Q
  if (p < 0)
    t = 0;
  Q[t++] = u;
  for (int i = 0; i < G.deg[u]; i++)
    if (G.list[u][i] != p)
      dfs(G, G.list[u][i], Q, u);
}

double tsp_mst(point *V, int n, int *Q, graph T) {
  // Algorithme:
  // 1. remplir puis trier le tableau d'arêtes
  // 2. répéter pour chaque arête u-v, mais pas plus de n-1 fois:
  //    si u-v ne forme pas un cycle de T (<=> u,v dans des composantes
  //    différentes) alors ajouter u-v au graphe T
  // 3. calculer dans Q le DFS de T

  // E = tableau de toutes les arêtes définies à partir des n points de V
  edge *E = malloc(sizeof(*E) * n*(n-1)/2);
  int u, v, t, x, y;

  // remplit E[]
  for (u = t = 0; u < n; u++) // solution alternative: "u<n-1"
    for (v = u + 1; v < n; v++)
      E[t++] = (edge){ .u = u, .v = v, .weight = dist(V[u], V[v]) };

  // trie les arêtes de E[]
  qsort(E, t, sizeof(*E), compEdge);

  int *parent = malloc(n * sizeof(int)); // parent[u]=parent de u (=u si racine)
  int *rank = malloc(n * sizeof(int));   // rank[u]=rang de l'arbre de racine u
  for (int u = 0; u < n; u++) {
    parent[u] = u; // chacun est racine de son propre arbre
    rank[u] = 0;   // rang nul
    T.deg[u] = 0;  // pour les appels multiples à tsp_mst(...,T)
  }

  t = 0;         // indice dans E
  int k = n - 1; // k = #arêtes qui restent à ajouter à T

  while (k) { // tant qu'il reste une arête à ajouter à T
    x = Find(E[t].u, parent);
    y = Find(E[t].v, parent);
    if (x != y) {
      Union(x, y, parent, rank);
      addEdge(T, E[t].u, E[t].v);
      k--;
    }
    t++;
  }

  // libère les tableaux devenus inutiles
  free(parent);
  free(rank);
  free(E);

  dfs(T, 0, Q, -1);      // calcule Q grâce au DFS à partir du sommet 0 de T
  return value(V, n, Q); // renvoie la valeur de la tournée
}
