import sys
import glob
import os
import numpy as np


sommets = set()
def readfile(filename):
    global sommets
    seconds = []
    chapeaux = []
    aretes = []
    with open(filename) as f:
        for line in f:
            data = [str(x) for x in line.split()] 
            if "seconds" in data:
                seconds.append(float(data[1]))
            if "chapeaux" in data:
                chapeaux.append(int(data[6]))
            if not "réduit" in data and "sommets" in data:
                sommets.add(int(data[3]))
                aretes.append(int(data[6]))
    return seconds,chapeaux,aretes

def complete_l(l):
    for ll in l:
        while(len(ll) < 40):
            ll.append(-1)

def mean_ll(l):
    res = []
    for i in range(len(l[0])):
        m = 0
        cpt = 0
        tmp = []
        for ll in l:
            if ll[i] != -1:
                tmp.append(ll[i])
                cpt += 1
        res.append(np.median(tmp))
    return res


def main():
    dir = sys.argv[1]
    list_sec_total = []
    list_chapeaux_total = []
    list_aretes_total = []
    for file in os.listdir(dir):
        if not file.endswith(".out"):
            continue
        s,c,e = readfile(dir+"/"+file)
        list_sec_total.append(s)
        list_chapeaux_total.append(c)
        list_aretes_total.append(e)
    l = []
    complete_l(list_sec_total)
    complete_l(list_chapeaux_total)
    complete_l(list_aretes_total)

    m_c = mean_ll(list_chapeaux_total)
    m_s = mean_ll(list_sec_total)
    m_e = mean_ll(list_aretes_total)
    print(len(sommets)," sommets:")
    for i in sorted(sommets):
        print(i)
    print("moyenne des aretes par taille:")
    for i in m_e:
        print(i)
    print("moyenne des chapeaux par taille:")
    for i in m_c:
        print(i)

    print("moyenne des secondes par taille:")
    for i in m_s:
        print(i)
    return 0

if __name__ == "__main__":
    main()