from z3 import *

from itertools import combinations
import networkx as nx
import sys
import subprocess
import time as time

# -- reading Graph -- #


def readFile(filename):
    g = nx.Graph()
    with open(filename) as f:
        for line in f:
            data = [str(x) for x in line.split()]
            if data[0] == "v":
                g.add_node(int(data[1]), type=int(data[2]),
                           candidate=False, chapeau=False)
            if data[0] == "e":
                g.add_edge(int(data[1]), int(data[2]), hetero=False)

    return g


def set_hetero_edge(g):
    for v1, v2 in g.edges():
        if g.nodes[v1]["type"] != g.nodes[v2]["type"]:
            g[v1][v2]["hetero"] = True
            g.nodes[v1]["candidate"] = True
            g.nodes[v2]["candidate"] = True
        else:
            g[v1][v2]["hetero"] = False


# -- end reading Graph -- #

# -- Check result -- #

def try_chapeaux(g, red, n):
    print(g[n])
    mettre_chapeau(red, n)
    return check(g, red)


def mettre_chapeau(g, c):
    g.nodes[c]["chapeau"] = True


def check(g, red):
    chapeaux = [n for n in g.nodes() if red.nodes[n]["chapeau"] == True]
    # print("chapeaux:",chapeaux)
    for c in chapeaux:
        add_edges_connected(g, red, c)
    if not nx.is_connected(red):
        return False
    return True


def reset_chapeau(g):
    for n in g.nodes():
        g.nodes[n]["chapeau"] = False


def test_check(g, red, m, x_set):
    for u in g.nodes():
        for i in range(1, k):
            if m[x_set[(u, i)]]:
                mettre_chapeau(red, u)
    return check(g, red)


def add_edges_connected(g, red, c):
    neighs = g.edges(c)
    # print(neighs)
    red.add_edges_from(neighs)


# -- creation variables -- #


def create_x_set(g,x_set):
    for u in g.nodes():
        for i in range(k-1, k):
            x_set[(u, i)] = Bool("x_%i_%i" % (u, i))

    return x_set


def create_t_set(g):
    x_set = dict()
    for u, v in g.edges():
        x_set[(u, v)] = Bool("t_%i_%i" % (u, v))

    return x_set


def composante_connected(g, c1, c2):
    for u in c1:
        for v in c2:
            if g.has_edge(u, v):
                return 1
    return 0


def create_tree(g, cc):
    h = nx.DiGraph()
    for i in range(len(cc)):
        h.add_node(i, nodes=cc[i])
    for i in range(len(cc)):
        for j in range(len(cc)):
            if composante_connected(g, cc[i], cc[j]):
                h.add_edge(i, j)
    return h


def create_p(st):
    p_set = dict()
    for j, j2 in st.edges():
        p_set[(j, j2)] = Bool("p_%i_%i" % (j, j2))
    return p_set


def create_l(st, h):
    l_set = dict()
    for j in st.nodes():
        for i in range(h+1):
            l_set[(j, i)] = Bool("l_%i_%i" % (j, i))
    return l_set


def get_root(g):
    for node in g.nodes():
        if g.out_degree(node) == 0:
            return node


def set_level(g):
    root = get_root(g)
    level = nx.shortest_path_length(g, root)
    nx.set_node_attributes(g, level, 'levels')

# -- End creation variables -- #


def remove_useless(g):
    not_c = [n for n in g.nodes() if not g.nodes[n]["candidate"]]
    tmp = g.copy()

    g.remove_nodes_from(not_c)
    if len(g) == 0:
        print("Couleur unique.")
        print("On a donc un résultat avec 0 chapeaux")
        exit(0)

def get_hetero_edge(g):
    l = list()
    for v1, v2 in g.edges():
        if g[v1][v2]["hetero"]:
            l.append((v1, v2))
    return l


def compute_without_hetero(g):
    set_hetero_edge(g)
    edges = get_hetero_edge(g)

    tmp = g.copy()
    tmp.remove_edges_from(edges)
    return tmp


def at_most_one(literals):
    clauses = list()
    # All the possible pairs of distinct elements
    if len(literals) == 1:
        return clauses
    for comb in combinations(literals, 2):
        clauses += [Or(Not(comb[0]), Not(comb[1]))]

    return clauses


def exactly_one(literals):
    if len(literals) == 0:
        return []
    if len(literals) == 1:
        return literals
    clauses = [Or(literals)]

    # All the possible pairs of distinct elements
    for comb in combinations(literals, 2):
        clauses += [Or(Not(comb[0]), Not(comb[1]))]
    return clauses


def at_least_one(literals):
    return [Or(literals)]


def conv(u, v, y_set):
    if not y_set:
        return False
    l = []
    for j in range(1, k):
        l.append(y_set[(u, j)])
        l.append(y_set[(v, j)])

    return Or(l)


def get_p_j(st, p_set, j):
    return [p_set[(j, i)] for i in st[j]]


def get_y_i(g, y_set, i):
    return [y_set[(u, i)] for u in g.nodes()]


def get_x_i(x_set, u, l, s, t):
    return [x_set[(u, i, s, t)] for i in range(1, l+1)]


def get_x_u(g, x_set, i, s, t):
    return [x_set[(u, i, s, t)] for u in g.nodes()]


def one_clause(tmp):
    return [item for sublist in tmp for item in sublist]


def x_u(x_set, u):
    return [x_set[(u, i)] for i in range(1, k)]


def phi_each_p(g, t, j, j2, x_set, l_set):
    clause = []
    Xj = t.nodes()[j]["nodes"]
    Xj2 = t.nodes()[j2]["nodes"]
    for u in Xj:
        for v in Xj2:
            for i in range(k-1, k):
                clause.append(Or(x_set[u, i], x_set[v, i]))
    l_clause = []
    for i in range(h):
        l_clause.append(And(l_set[(j, i)], l_set[(j2, i+1)]))

    ajout = []

    for u in Xj:
        for v in Xj2:
            if g.has_edge(u, v):
                for i in range(1, k):
                    ajout.append(Or(x_set[u, i], x_set[v, i]))

    return And(Or(clause), Or(l_clause), Or(ajout))


def phi(g, t, x_set, p_set, l_set):
    clauses = []
    for j in range(1, h):
        clauses += exactly_one(get_p_j(t, p_set, j))
    for u in t.nodes():
        clauses += exactly_one(x_u(l_set, u))
    for i in range(1, k):
        clauses += at_most_one(get_y_i(g, x_set, i))

    for u in g.nodes():

        clauses += at_most_one(x_u(x_set, u))

    for j, j2 in list(p_set):
        clauses += [Implies(p_set[(j, j2)],
                            phi_each_p(g, t, j, j2, x_set, l_set))]

    return clauses

def complete_same_cc(g):
    tmp = compute_without_hetero(g)
    cc = [list(c) for c in nx.connected_components(tmp)]
    for c in cc:
        if len(c) > 2:
            for i in range(len(c)):
                for j in range(i+1,len(c)):
                    g.add_edge(c[i],c[j])



def main():
    global k
    global h
    print("start creation")
    t = time.time()
    g = readFile(sys.argv[1])
    k = 2

    set_hetero_edge(g)
    complete_same_cc(g)
    remove_useless(g)
    red = compute_without_hetero(g)
    if nx.is_empty(g):
        print("Couleur unique.")
        print("On a donc un résultat avec 0 chapeaux")
        return 0

    print("Graphe réduit avec", len(g), "sommets et",
          g.number_of_edges(), "arêtes.")


    cc = list(nx.connected_components(red))
    st = create_tree(g, cc)
    p_set = create_p(st)

    h = len(st)
    l_set = create_l(st, h)
    x_set = dict()
    clauses = []
    while(True):
        print("k=", k-1)
        create_x_set(g,x_set)
        clauses = phi(g, st, x_set, p_set,l_set)
        # print(clauses)
        s = Solver()

        for clause in clauses:
            s.add(clause)

        launch = time.time()
        if str(s.check()) == 'sat':
            break

        k += 1

    print("sat")
    m = s.model()

    cpt = 0
    for u in g.nodes():
        for i in range(1, k):
            if m[x_set[(u, i)]]:
                mettre_chapeau(red, u)
                print("chapeau sur le sommet", u, "à la position",
                      i, ",variable:", x_set[(u, i)])
                cpt += 1

    print(cpt, "chapeaux ont été trouvé")

    print("Temps de résolution:", time.time()-launch)
    print('Temps total:', time.time()-t, "secondes")
    return 0


if __name__ == "__main__":
    main()
