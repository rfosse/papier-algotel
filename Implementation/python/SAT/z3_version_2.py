

from z3 import *

from itertools import combinations
import networkx as nx
import sys
import subprocess
import time as time
sys.path.append('../')
import reduction
# -- reading Graph -- #


def readFile(filename):
	g = nx.Graph()
	with open(filename) as f:
		for line in f:
			data = [str(x) for x in line.split()]
			if data[0] == "v":
				g.add_node(int(data[1]), type=int(data[2]),
						   candidate=False)
			if data[0] == "e":
				g.add_edge(int(data[1]), int(data[2]), hetero=False)

	return g


def set_hetero_edge(g):
	for v1, v2 in g.edges():
		if g.nodes[v1]["type"] != g.nodes[v2]["type"]:
			g[v1][v2]["hetero"] = True
			g.nodes[v1]["candidate"] = True
			g.nodes[v2]["candidate"] = True
		else:
			g[v1][v2]["hetero"] = False


# -- end reading Graph -- #

# -- creation variables -- #


def create_x_set(g):
	x_set = dict()

	for u in g.nodes():
		for s in g.nodes():
			for t in g.nodes():
				if s >= t:
					continue
				for l in range(0, len(g)+1):
					x_set[(u, l, s, t)] = Bool("x_%i_%i_%i_%i" % (u, l, s, t))

	return x_set


def create_y_set(g, y_set):
	for u in g.nodes():
		for i in range(k-1, k):
			y_set[(u, i)] = Bool("y_%i_%i" % (u, i))


# -- End creation variables -- #


def at_most_one(literals):
	clauses = list()
	# All the possible pairs of distinct elements
	if len(literals) == 1:
		return clauses
	for comb in combinations(literals, 2):
		clauses += [Or(Not(comb[0]), Not(comb[1]))]

	return clauses


def exactly_one(literals):
	clauses = [[Or(literals)]]

	# All the possible pairs of distinct elements
	for comb in combinations(literals, 2):
		clauses += [[Or(Not(comb[0]), Not(comb[1]))]]
	return clauses


def at_least_one(literals):
	return [[Or(literals)]]


def conv(u, v, y_set):
	if not y_set:
		return False
	l = []
	for j in range(1, k):
		l.append(y_set[(u, j)])
		l.append(y_set[(v, j)])

	return Or(l)


def sub_path(g, s, t, l, x_set, y_set):
	cl = list()
	for i in range(1, l+1):
		lis = []
		for u in g.nodes():
			x = x_set[(u, i, s, t)]

			for v in g[u]:
				n = []

				if not g[u][v]["hetero"]:
					n.append(x_set[(v, i+1, s, t)])
				else:
					n.append(And(x_set[(v, i+1, s, t)], conv(u, v, y_set)))

			for y in n:
				lis.append(And(x, y))

		cl.append(lis)

	return cl


def sub_path_networkx(g, s, t, p, x_set, y_set):
	cl = []
	cl.append((x_set[(s, 0, s, t)]) == True)
	cl.append((x_set[(s, len(p), s, t)]) == True)

	for i in range(len(p)-1):
		x = x_set[(p[i], i, s, t)]
		if g[p[i]][p[i+1]]["hetero"]:
			cl.append(
				And(x, And(x_set[(p[i+1], i+1, s, t)], conv(p[i], p[i+1], y_set))))
			break
		else:
			cl.append(And(x, x_set[(p[i+1], i+1, s, t)]))

	return And(cl)


def path(g, x_set, y_set):
	clauses = list()
	tmp = list()

	for s in g.nodes():
		for t in g.nodes():
			p_list = list()
			if s >= t:
				continue
			paths = nx.all_simple_paths(g, source=s, target=t)
			cpt = 0
			for p in paths:
				p_i = Bool("p_%i_%i_%i" % (s, t, cpt))
				tmp.append(sub_path_networkx(g, s, t, p, x_set, y_set) == p_i)
				p_list.append(p_i)
				cpt += 1
			clauses += at_least_one(p_list)
	clauses += tmp

	return clauses


def get_y_u(y_set, u):
	return [y_set[(u, i)] for i in range(1, k)]


def get_y_i(g, y_set, i):
	return [y_set[(u, i)] for u in g.nodes()]


def get_x_i(x_set, u, l, s, t):
	return [x_set[(u, i, s, t)] for i in range(1, l+1)]


def get_x_u(g, x_set, i, s, t):
	return [x_set[(u, i, s, t)] for u in g.nodes()]


def one_clause(tmp):
	return [item for sublist in tmp for item in sublist]


def phi(g, x_set, y_set):
	clauses = []
	for u in g.nodes():
		clauses += at_most_one(get_y_u(y_set, u))

	for i in range(k-1, k):
		clauses += exactly_one(get_y_i(g, y_set, i))

	for s in g.nodes():
		for t in g.nodes():
			if s >= t:
				continue
			dis = []
			for l in range(1, nx.diameter(g)):
				tmp = []
				for u in g.nodes():
					x = get_x_i(x_set, u, l, s, t)
					tmp += [at_most_one(x)]

				for i in range(0, l+1):
					tmp += [at_most_one(get_x_u(g, x_set, i, s, t))]

				dis.append(one_clause(tmp))
			clauses += dis

	clauses += path(g, x_set, y_set)

	return clauses


def compute_without_hetero(g):
	set_hetero_edge(g)
	edges = reduction.get_hetero_edge(g)

	tmp = g.copy()
	tmp.remove_edges_from(edges)
	return tmp


def remove_useless(g):
	not_c = [n for n in g.nodes() if not g.nodes[n]["candidate"]]
	tmp = g.copy()

	g.remove_nodes_from(not_c)
	if len(g) == 0:
		print("Couleur unique.")
		print("On a donc un résultat avec 0 chapeaux")
		exit(0)


def complete_same_cc(g):
	tmp = compute_without_hetero(g)
	cc = [list(c) for c in nx.connected_components(tmp)]
	for c in cc:
		if len(c) > 2:
			for i in range(len(c)):
				for j in range(i+1, len(c)):
					g.add_edge(c[i], c[j], hetero=False)


def main():
	global k
	print("start creation")
	t = time.time()
	g = readFile(sys.argv[1])
	assert(nx.number_connected_components(g) == 1)
	k = 2
	set_hetero_edge(g)
	complete_same_cc(g)
	remove_useless(g)

	assert(nx.number_connected_components(g) == 1)
	print("new size:", len(g))
	print("generation sets in:", time.time()-t)
	launch = time.time()
	x_set = create_x_set(g)
	y_set = dict()
	clauses = []
	while(True):
		print(k)
		create_y_set(g,y_set)

		clauses = phi(g, x_set, y_set)
		s = Solver()
		for clause in clauses:
			s.add(clause)

		if str(s.check()) == 'sat':
			break

		k += 1
	if str(s.check()) == 'sat' and s.model():
		print("sat")
		m = s.model()
		cpt = 0
		for u in g.nodes():
			for i in range(1, k):
				if m[y_set[(u, i)]]:
					print("chapeaux sur le sommet", u, "à la position",
						  i, ",variable:", y_set[(u, i)])
					cpt += 1
		print(cpt, "chapeaux ont été trouvé")
	else:
		print("unsat")
	print("Temps de résolution:", time.time()-launch)
	print('Temps total:', time.time()-t, "secondes")
	return 0


if __name__ == "__main__":
	main()
