import sys
import networkx as nx
import collections
import subprocess

# TODO implementation des regles de logique; transformation de ces regles en clauses CNF; experimentation dans Glucose; Essai et difference performance avec FPT.

# Plus facile de les declarer en globale, je modifierai apres
nb_var = 0
nb_clause = 0

set_variables = list()
list_convertisseurs = list()
set_clauses = list() #sale mais je modifierai après
list_l = list()


# On suppose ici que le fichier d'entree a le format suivant :
# v index { 0 | 1 }
# e v1 v2 (si il existe une arête entre v1 et v2)
def readFile(filename):
    g = nx.Graph()
    with open(filename) as f:
        for line in f:
            data = [str(x) for x in line.split()] 
            if data[0] == "v":
                g.add_node(int(data[1]),type=int(data[2]),candidate=False,chapeau=False)
            if data[0] == "e":
                g.add_edge(int(data[1]),int(data[2]),hetero=False)

    return g

def set_hetero_edge(g):
    for v1,v2 in g.edges():
        if g.nodes[v1]["type"] != g.nodes[v2]["type"]:
            g[v1][v2]["hetero"] = True
            g.nodes[v1]["candidate"] = True
            g.nodes[v2]["candidate"] = True
        else:
            g[v1][v2]["hetero"] = False


# on extrait un candidat c, celui relié à le plus de composante connexe

def super_degree(g,red,n):
    s = set()
    for neigh in g[n]:

        s.add(tuple(list(nx.node_connected_component(red,neigh)))) # check complexité


    return len(s)-1


def compute_without_hetero(g):
    set_hetero_edge(g)
    edges = get_hetero_edge(g)

    tmp = g.copy()
    tmp.remove_edges_from(edges)
    return tmp


def is_hetero(g,v1,v2):
    return g[v1][v2]["hetero"]

def print_hetero_edge(g):
    for v1,v2 in g.edges():
        if g[v1][v2]["hetero"]:
            print(v1,v2)

def get_hetero_edge(g):
    l = list()
    for v1,v2 in g.edges():
        if g[v1][v2]["hetero"]:
            l.append((v1,v2))
    return l


def return_node_type(g,node):
    return g.nodes[node]["type"]

def nodes_by_type(g):
    type1 = list()
    type2 = list()
    res = (type1,type2)
    for node in g.nodes():
        res[return_node_type(g,node)].append(node)
    return res

def remove_useless(g,tmp):
    remove = []
    for v in tmp.nodes():
        if not g.nodes[v]["candidate"]:
            remove.append(v)
    tmp.remove_nodes_from(remove)

def hetero_couple(g):
    res = []
    for v1,v2 in g.edges():
        if g[v1][v2]["hetero"]:
            res.append((v1,v2))
    return res

def nodes_as_str(g):
    string = " "
    for v in g.nodes():
        string = string + str(v) + " "
    return string

def composantes(g,type):
    tmp = g.copy()
    tmp.remove_nodes_from(type)
    remove_useless(g,tmp)
    return tmp

def reduct_graph(g):
    type = nx.get_node_attributes(g,"type")
    type0 = [i for i,val in type.items() if val == 0]
    type1 = [i for i,val in type.items() if val == 1]
    tmp0 = composantes(g,type0)
    tmp1 = composantes(g,type1)
    graph_res = nx.compose(tmp0,tmp1)
    graph_res.add_edges_from(hetero_couple(g))
    set_hetero_edge(graph_res)
    return graph_res


def init_conv(g,k):
    global list_convertisseurs
    for n in g.nodes():
        for i in range(1,k+1):
            list_convertisseurs.append(n,i)


def init_l(g,components,k):
    global list_l
    for j in range(len(components)):
        for i in range(0,k+1):
            list_l.append((j,i))


def p_j_j2(g,components,k,j,j2):
    X_j = components[j]
    X_j2 = components[j2]
    x_list = list()
    l_list = list()
    for u in X_j:
        for v in X_j2:
            for i in range(1,k+1):
                x_list.append((u,i))
                x_list.append((v,i))
    
    for i in range(0,k):
        l_list.append((j,i))
        l_list.append((j2,i+1))
    
    return 0

def main():
    l = 3
    k = 2

    g = readFile(sys.argv[1])
    set_hetero_edge(g)
    reduct = reduct_graph(g)
    red = compute_without_hetero(reduct)
    components = list(nx.connected_components(red))
    print(components)
    init_conv(g,k)

    return 0

if __name__ == "__main__":
    main()