import reduction as lib
import sys
#import z3

def all_paths(g,source,target,l):
    return lib.reachability(g,source,target,l)


def main():
    g = lib.readFile(sys.argv[1])
    lib.set_hetero_edge(g)
    reduct = lib.reduct_graph(g)

    lib.print_graph(reduct)
    lib.print_hetero_edge(reduct)
    
    paths = all_paths(reduct,1,4,3)
    print(paths)

    lib.print_cnf("example_cnf.cnf")



if __name__ == "__main__":
    main()