import sys
import networkx as nx
from graph import *

current_solution = list()
best_solution = list()
def super_degree(g,red,n):
    s = set()
    for neigh in g[n]:

        s.add(tuple(list(nx.node_connected_component(red,neigh)))) # check complexité

    return len(s)-1

def get_better_candidate(g,red,candidates):
    res = 0
    n_max = 0
    for n in candidates:
        sd = super_degree(g,red,n)
        if res < sd:
            res = sd
            n_max = n

    return n_max

def bandb(g,red):
    candidates = [n for n in g.nodes() if g.nodes[n]["candidate"] == True]
    while not nx.is_connected(red):
        c = get_better_candidate(g,red,candidates)
        current_solution.append(c)
        mettre_chapeau(red,c,candidates)
        add_edges_connected(g,red,c)

    return current_solution




def main():
    g = readFile(sys.argv[1])
    red = compute_without_hetero(g)
    bandb(g,red)
    return 0



if __name__ == "__main__":
    main()