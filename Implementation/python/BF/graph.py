import math
import networkx as nx
import matplotlib.pyplot as plt


def readFile(filename):
    g = nx.Graph()
    with open(filename) as f:
        for line in f:
            data = [str(x) for x in line.split()] 
            if data[0] == "v":
                g.add_node(int(data[1]),type=int(data[2]),candidate=False,chapeau=False)
            if data[0] == "e":
                g.add_edge(int(data[1]),int(data[2]),hetero=False)

    return g


def set_hetero_edge(g):
    for v1,v2 in g.edges():
        if g.nodes[v1]["type"] != g.nodes[v2]["type"]:
            g[v1][v2]["hetero"] = True
            g.nodes[v1]["candidate"] = True
            g.nodes[v2]["candidate"] = True
        else:
            g[v1][v2]["hetero"] = False

def get_hetero_edges(g):
    res = list()
    for v1,v2 in g.edges():
        if g[v1][v2]["hetero"]:
            res.append((v1,v2))
    return res


def mettre_chapeau(g,c,candidates):
    g.nodes[c]["chapeau"] = True
    candidates.remove(c)

def add_edges_connected(g,red,c):
    neighs = g.edges(c)

    red.add_edges_from(neighs)


def reset_chapeau(g):
    for n in g.nodes():
        g.nodes[n]["chapeau"] = False

        
def check(g,red):
    chapeaux = [n for n in g.nodes() if red.nodes[n]["chapeau"] == True]
    #print("chapeaux:",chapeaux)
    for c in chapeaux:
        add_edges_connected(g,red,c)
    if not nx.is_connected(red):
        return False
    return True


def compute_without_hetero(g):
    set_hetero_edge(g)
    edges = get_hetero_edges(g)

    tmp = g.copy()
    tmp.remove_edges_from(edges)
    return tmp
