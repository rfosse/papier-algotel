import sys
import reduction
import networkx as nx
from itertools import combinations
import time
import copy
from networkx.drawing.nx_agraph import write_dot


def readFile(filename):
    g = nx.Graph()
    with open(filename) as f:
        for line in f:
            data = [str(x) for x in line.split()]
            if data[0] == "v":
                g.add_node(int(data[1]), type=int(data[2]),
                           candidate=False, chapeau=False)
            if data[0] == "e":
                g.add_edge(int(data[1]), int(data[2]), hetero=False)

    return g


def set_hetero_edge(g):
    for v1, v2 in g.edges():
        if g.nodes[v1]["type"] != g.nodes[v2]["type"]:
            g[v1][v2]["hetero"] = True
            g.nodes[v1]["candidate"] = True
            g.nodes[v2]["candidate"] = True
        else:
            g[v1][v2]["hetero"] = False


def mettre_chapeau(g, c):
    g.nodes[c]["chapeau"] = True


def reset_chapeau(g):
    for n in g.nodes():
        g.nodes[n]["chapeau"] = False


def add_edges_connected(g, red, c):
    neighs = g.edges(c)
    # print(neighs)
    red.add_edges_from(neighs)


def check(g, red):
    chapeaux = [n for n in g.nodes() if red.nodes[n]["chapeau"] == True]
    for c in chapeaux:
        add_edges_connected(g, red, c)
    if not nx.is_connected(red):
        return False
    return True


def compute_without_hetero(g):
    edges = reduction.get_hetero_edge(g)

    tmp = g.copy()
    tmp.remove_edges_from(edges)
    return tmp


def bruteForce(g):
    candidates = [n for n in g.nodes() if g.nodes[n]["candidate"] == True]
    i = 1
    #original_tab = tab_subconnected(red)
    while i <= len(candidates):
        comb = combinations(candidates, i)
        for c in comb:
            red = compute_without_hetero(g)

            reset_chapeau(red)

            for x in c:
                mettre_chapeau(red, x)
            if check(g, red):
                return c

        i += 1


def tab_subconnected(red):
    cc = list(nx.connected_components(red))
    tab = dict()
    for i in range(len(red)):
        for j in range(len(cc)):
            if i in cc[j]:
                tab[i] = j
                break

    return tab


def remove_useless(g):
    not_c = [n for n in g.nodes() if not g.nodes[n]["candidate"]]
    tmp = g.copy()

    g.remove_nodes_from(not_c)
    if len(g) == 0:
        print("Couleur unique.")
        print("On a donc un résultat avec 0 chapeaux")
        exit(0)


def try_chapeaux(g, red, n):
    print(g[n])
    mettre_chapeau(red, n)
    return check(g, red)

def complete_same_cc(g):
    cc = [list(c) for c in nx.connected_components(g)]
    for c in cc:
        if len(c) > 2:
            for i in range(len(c)):
                for j in range(i+1,len(c)):
                    g.add_edge(c[i],c[j])


def main():
    start_time = time.time()
    g = readFile(sys.argv[1])
    set_hetero_edge(g)
    red = compute_without_hetero(g)
    complete_same_cc(red)
    print("Graphe d'origine avec", len(g),
          "sommets et", g.number_of_edges(), "arêtes.")
    remove_useless(red)

    print("Graphe réduit avec", len(red), "sommets et",
          red.number_of_edges(), "arêtes.")
    print("Nombre de composantes connexes dans le graphe réduit:",
          nx.number_connected_components(red))

    res = bruteForce(g)
    print("On a donc un résultat avec", len(res), "chapeaux qui sont:", res)
    print("--- %s seconds ---" % (time.time() - start_time))

    #write_dot(red, "bruteForce.dot")
    # reduction.tulip_file(red,"out.tlp")
    return 0


if __name__ == "__main__":
    main()
