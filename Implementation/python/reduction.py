import sys
import networkx as nx
import collections
import subprocess

# TODO implementation des regles de logique; transformation de ces regles en clauses CNF; experimentation dans Glucose; Essai et difference performance avec FPT.

# Plus facile de les declarer en globale, je modifierai apres
nb_var = 0
nb_clause = 0

set_variables = list()
set_convertisseurs = set()
set_clauses = list() #sale mais je modifierai après
l = 2 # Pour le test, a virer apres
k = 1


class Variable:
    def __init__(self,node,position,start=None,target=None):
        self.node = node
        self.position = position
        self.start = start
        self.target = target
        self.number = self.set_number() # Le numero de la variable pour l'ecriture dans la CNF
        self.value = False
        self.name = self.set_name(node,position,start,target)
    def get_position(self):
        return self.position

    def set_position(self,position):
        self.position = position

    def set_number(self):
        global nb_var
        nb_var+=1
        return nb_var

    def get_number(self):
        return self.number

    def get_node(self):
        return self.node

    def get_value(self):
        return self.value

    def set_value(self,value):
        self.value = value

    def set_name(self,node,position,start="",target=""):
        return str(node)+"-"+str(position)+"-"+str(start)+"-"+str(target)

    def get_name(self):
        return self.name
class Clause:
    def __init__(self):
        self.id = self.set_id()
        self.lits = []

    def get_lits(self):
        return self.lits
    
    def set_lits(self,lits):
        self.lits = lits

    def add_var(self,var):
        self.lits.append(var)

    def remove_var(self,var):
        assert(var in self.lits)
        self.lits.remove(var)

    def get_id(self):
        return self.id
    
    def set_id(self):
        global nb_clause
        nb_clause += 1
        return nb_clause

    def print_clause(self):
        for var in self.lits:
            if var[1]: # positive
                print(var[0].get_number(),end=" ")
            else:
                print(var[0].get_number()*-1,end=" ")
        print("0")

# On suppose ici que le fichier d'entree a le format suivant :
# v index { 0 | 1 }
# e v1 v2 (si il existe une arête entre v1 et v2)
def readFile(filename):
    g = nx.Graph()
    with open(filename) as f:
        for line in f:
            data = [str(x) for x in line.split()] 
            if data[0] == "v":
                g.add_node(int(data[1]),type=int(data[2]),candidate=False,chapeau=False)
            if data[0] == "e":
                g.add_edge(int(data[1]),int(data[2]),hetero=False)

    return g

def set_hetero_edge(g):
    for v1,v2 in g.edges():
        if g.nodes[v1]["type"] != g.nodes[v2]["type"]:
            g[v1][v2]["hetero"] = True
            g.nodes[v1]["candidate"] = True
            g.nodes[v2]["candidate"] = True
        else:
            g[v1][v2]["hetero"] = False

def is_hetero(g,v1,v2):
    return g[v1][v2]["hetero"]

def print_hetero_edge(g):
    for v1,v2 in g.edges():
        if g[v1][v2]["hetero"]:
            print(v1,v2)

def get_hetero_edge(g):
    l = list()
    for v1,v2 in g.edges():
        if g[v1][v2]["hetero"]:
            l.append((v1,v2))
    return l


def return_node_type(g,node):
    return g.nodes[node]["type"]

def nodes_by_type(g):
    type1 = list()
    type2 = list()
    res = (type1,type2)
    for node in g.nodes():
        res[return_node_type(g,node)].append(node)
    return res

def remove_useless(g,tmp):
    remove = []
    for v in tmp.nodes():
        if not g.nodes[v]["candidate"]:
            remove.append(v)
    tmp.remove_nodes_from(remove)

def hetero_couple(g):
    res = []
    for v1,v2 in g.edges():
        if g[v1][v2]["hetero"]:
            res.append((v1,v2))
    return res

def nodes_as_str(g):
    string = " "
    for v in g.nodes():
        string = string + str(v) + " "
    return string

def reachability(g,source,target,l):
    paths = list(nx.all_simple_paths(g,source,target,cutoff=l)) # Trie par ordre decroissant de taille
    return paths

def create_clauses_paths(path):
    global set_clauses
    global set_variables
    for p in path:
        res = list()
        for i in range(1,len(p)): #On connait déjà le 0 et l (rappel range va jusqu'à l-1)
            tmp = (Variable(p[i],i,p[0],p[-1]),True)
            set_variables.append(tmp[0])
            res.append(tmp)
        c = Clause()
        c.set_lits(res)
        set_clauses.append(c)

def add_convertisseurs(var):
    add = False
    global set_convertisseurs
    if len(set_convertisseurs) == 0:
        set_convertisseurs.add(var)
        return 0


    for v in set_convertisseurs:
        if var.get_node() == v.get_node() and var.get_position() == v.get_position():
            add = False
            break
        if var.get_node() == v.get_node() and var.get_position() == v.get_position():
            unique_duo(var,v)
            add = True
        if var.get_node() != v.get_node() and var.get_position() != v.get_position():
            add = True

    if add:
        set_convertisseurs.add(var)
            

def add_unary_clause(var,polarity):
    global set_clauses
    c = Clause()
    c.add_var((var,polarity))
    set_clauses.append(c)

def get_edges_path(path):
    res = list()
    for p in path:
        for i in range(0,len(p)-1):
            res.append((p[i],p[i+1]))
    return res

def at_least_one_conv(var1,var2):
    global set_clauses
    c = Clause()
    c.set_lits([(var1,True),(var2,True)])
    set_clauses.append(c)

    c = Clause()
    c.set_lits([(var1,True),(var2,False)])
    set_clauses.append(c)

    c = Clause()
    c.set_lits([(var1,False),(var2,True)])
    set_clauses.append(c)


def conv(g,v1,v2): # k est le nombre de convertisseur
    global set_clauses
    tmp = list()
    c = Clause()
    for i in range(1,k+1):
        var1 = Variable(v1,i)
        var2 = Variable(v2,i)
        tmp.append((var1,True))
        tmp.append((var2,True))
        add_convertisseurs(var1)
        add_convertisseurs(var2)

        at_least_one_conv(var1,var2)
    c.set_lits(tmp)
    set_clauses.append(c)

def unique_duo(var1,var2):
    global set_clauses

    c = Clause()
    c.set_lits([(var1,False),(var2,True)])
    set_clauses.append(c)
    c = Clause()
    c.set_lits([(var1,True),(var2,False)])
    set_clauses.append(c)

    c = Clause()
    c.set_lits([(var1,False),(var2,False)])
    set_clauses.append(c)


def unique(set_var):
    global set_clauses
    c = Clause()
    for i in set_var:
        c.add_var((i,False))
    set_clauses.append(c)
    c = Clause()
    for i in set_var:
        c.add_var((i,True))
    set_clauses.append(c)
    for i in range(len(set_var)):

        for j in range(i+1,len(set_var)):
            tmp = list()
            c = Clause()
            tmp.append((set_var[i],False))
            tmp.append((set_var[j],True))
            c.set_lits(tmp)
            set_clauses.append(c)


def tulip_file(g,filename):
    cpt=0
    with open(filename,"w") as f:
        f.write("(tlp \"2.0\" \n")
        f.write("(nodes"+nodes_as_str(g)+")\n")
        for v1,v2 in g.edges():
            f.write("(edge "+str(cpt)+" "+str(v1)+" "+str(v2)+")\n")
            cpt+=1

        f.write("(property 0 int \"type\" \n")
        for v in g.nodes():
            f.write("(node "+str(v)+" \""+str(g.nodes[v]["type"])+"\")\n")
        
        f.write("(property 0 bool \"candidate\" \n")
        for v in g.nodes():
            f.write("(node "+str(v)+" \""+str(g.nodes[v]["candidate"])+"\")\n")

        f.write("(property 0 bool \"chapeau\" \n")
        for v in g.nodes():
            f.write("(node "+str(v)+" \""+str(g.nodes[v]["chapeau"])+"\")\n")

def composantes(g,type):
    tmp = g.copy()
    tmp.remove_nodes_from(type)
    remove_useless(g,tmp)
    return tmp

def reduct_graph(g):
    type = nx.get_node_attributes(g,"type")
    type0 = [i for i,val in type.items() if val == 0]
    type1 = [i for i,val in type.items() if val == 1]
    tmp0 = composantes(g,type0)
    tmp1 = composantes(g,type1)
    graph_res = nx.compose(tmp0,tmp1)
    graph_res.add_edges_from(hetero_couple(g))
    set_hetero_edge(graph_res)
    return graph_res



def print_graph(g):
    print("print graph with",len(g),"nodes and",g.number_of_edges(),"edges.")
    for v in g.nodes():
        print("v ",v,g.nodes[v]["type"])
    for v1,v2 in g.edges():
        print("e ",v1,v2)

def print_clauses():
    for clause in set_clauses:
        clause.print_clause()

def print_cnf(filename):
    with open(filename,"w") as f:
        for clause in set_clauses:
            for var in clause.lits:
                if var[1]: # positive
                    f.write(str(var[0].get_number())+" ")
                else:
                    f.write(str(var[0].get_number()*-1)+" ")
            f.write("0\n")


def print_convertisseurs(set_convertisseurs):
    print(len(set_convertisseurs),"convertisseurs:")
    for v in set_convertisseurs:
        print(v.get_number(),": node",v.get_node()," at position",v.get_position(),": ",v.get_value())

def check_convertisseur():
    conv_valid = [v for v in set_convertisseurs if v.get_value() == True]
    print_convertisseurs(conv_valid)
    print("res conv:",len(conv_valid),"over ",len(set_convertisseurs),". Required is ",k)
    return 0


def parse_result():
    with open("solution.txt","r") as f:
        for l in f:
            if str(l.split()[0]) == "UNSAT":
                print("UNSAT result")
                return 1
            lits = [int(line) for line in l.split()]

    for conv in set_convertisseurs:
        if conv.get_number() in lits:
            conv.set_value(True)
        else:
            conv.set_value(False)

    check_convertisseur()
    #print_convertisseurs(set_convertisseurs)
    return 0

def reset_all():
    global set_clauses
    global set_variables
    global set_convertisseurs
    global nb_var
    global nb_clause
    set_clauses = [] 
    set_variables = []
    set_convertisseurs = set()
    nb_var = 0
    nb_clause = 0


def unique_position_conv():
    for v1 in set_convertisseurs:
        for v2 in set_convertisseurs:
                if v1.get_node() == v2.get_node() and v1.get_position() != v2.get_position():
                    unique_duo(v1,v2)


def chemin(g,source,target):
    x_s = Variable(source,0,source,target)
    x_t = Variable(source,l,source,target)
    add_unary_clause(x_s,True)
    add_unary_clause(x_t,True)


    for i in range(0,l+1):
        for w in g.nodes():
            x = Variable(w,i,source,target)
            for w2 in g.neighbors(w):
                y = Variable(w2,i+1,source,target)
                c = Clause()
                c.set_lits([(x,True),(y,True)])
                if is_hetero(g,w,w2):
                    conv(g,w,w2)

    return 1

#------------ NEW FUNCTIONS ---------------#

def init_convertisseur(g,k):
    global set_convertisseurs
    for n in g.nodes():
        for i in range(1,k+1):
            conv = Variable(n,i)
            set_convertisseurs.add(conv)

def init_x_var(g,k):
    for s in g.nodes():
        for t in g.nodes():
            if s == t:
                continue
            for u in g.nodes():
                for i in range(1,len(g)-1):
                    set_variables.append(Variable(u,i,s,t))

def uniq(list_x):
    global set_clauses
    c = Clause()
    for x in list_x:
        c.add_var((x,True))
    set_clauses.append(c)

    for xi in range(1,len(list_x)+1):
        for xj in range(1,len(list_x)+1):
            if xi >= xj:
                continue

            c = Clause()
            c.set_lits([(xi,False),(xj,True)])
            set_clauses.append(c)


def AtMostOne(list_x):

    for xi in range(1,len(list_x)+1):
        for xj in range(1,len(list_x)+1):
            if xi >= xj:
                continue

            c = Clause()
            c.set_lits([(xi,False),(xj,True)])
            set_clauses.append(c)


def find_pos_var_path(u,i,s,t):
    for var in set_variables:
        if var.get_name() == str(u)+"-"+str(i)+"-"+str(s)+"-"+str(t):
            return var

def path(g,l,s,t):
    x_0 = find_pos_var_path(s,0,s,t)
    add_unary_clause(x_0,True)
    x_t = find_pos_var_path(t,l,s,t)
    add_unary_clause(x_t,True)




           
def main():
    global l
    global k

    g = readFile(sys.argv[1])
    set_hetero_edge(g)

    reduct = reduct_graph(g)
    types = nodes_by_type(reduct)
    #tulip_file(reduct,"test.tlp")
    #print_graph(reduct)


    reset_all()
    print("Launch with k =",k,"l=",l)
    nodes = g.nodes()
    #print(nodes)

    chemin(reduct,0,4)
    print_convertisseurs(set_convertisseurs)
    print_cnf("example_cnf.cnf")
    exit(0)

    print_convertisseurs(set_convertisseurs)
    unique_position_conv()
    print_cnf("example_cnf.cnf")
    #print_clauses()
    subprocess.call("./launch_glucose.sh")
    parse_result()

    return 0

if __name__ == "__main__":
    main()