import reduction
import networkx as nx
import heapq
import sys
import time

def readFile(filename):
    g = nx.Graph()
    with open(filename) as f:
        for line in f:
            data = [str(x) for x in line.split()] 
            if data[0] == "v":
                g.add_node(int(data[1]),type=int(data[2]),candidate=False,chapeau=False)
            if data[0] == "e":
                g.add_edge(int(data[1]),int(data[2]),hetero=False)

    return g

def complete_same_cc(g):
    cc = [list(c) for c in nx.connected_components(g)]
    for c in cc:
        if len(c) > 2:
            for i in range(len(c)):
                for j in range(i+1,len(c)):
                    g.add_edge(c[i],c[j])



def remove_useless(g):
    remove = []
    for v in g.nodes():
        if not g.nodes[v]["candidate"]:
            remove.append(v)
    g.remove_nodes_from(remove)


def composantes(g,type):
    tmp = g.copy()
    tmp.remove_nodes_from(type)
    remove_useless(g)
    return tmp


def set_hetero_edge(g):
    for v1,v2 in g.edges():
        if g.nodes[v1]["type"] != g.nodes[v2]["type"]:
            g[v1][v2]["hetero"] = True
            g.nodes[v1]["candidate"] = True
            g.nodes[v2]["candidate"] = True
        else:
            g[v1][v2]["hetero"] = False



# on extrait un candidat c, celui relié à le plus de composante connexe

def super_degree(g,red,n):
    s = set()
    for neigh in g[n]:

        s.add(tuple(list(nx.node_connected_component(red,neigh)))) # check complexité


    return len(s)-1


def compute_without_hetero(g):
    set_hetero_edge(g)
    edges = reduction.get_hetero_edge(g)

    tmp = g.copy()
    tmp.remove_edges_from(edges)
    return tmp

def get_better_candidate(g,red,candidates):
    res = super_degree(g,red,candidates[0])
    n_max = candidates[0]
    for n in candidates[1:]:
        sd = super_degree(g,red,n)
        if res < sd:
            res = sd
            n_max = n

    return n_max


def mettre_chapeau(g,c,candidates):
    g.nodes[c]["chapeau"] = True
    candidates.remove(c)

def add_edges_connected(g,red,c):
    neighs = g.edges(c)

    red.add_edges_from(neighs)

def greedy(g,ht,hm):
    red = nx.Graph()
    
    red.add_nodes_from(g.nodes(),chapeau=False)
    red.add_edges_from(hm)
    candidates = [v1 for v1,v2 in ht]
    candidates += [v2 for v1,v2 in ht]
    candidates = list(set(candidates))
    while not nx.is_connected(red) and candidates:
        c = get_better_candidate(g,red,candidates)
        mettre_chapeau(red,c,candidates)
        add_edges_connected(g,red,c)
    return red


def tab_subconnected(red):
    cc = list(nx.connected_components(red))
    tab = dict()
    for i in range(len(red)):
        for j in range(len(cc)):
            if i in cc[j]:
                tab[i] = j
                break

    return tab

def reduce_graph(h,ht,hm):
	g=h.copy()
	g.remove_edges_from(ht)
	ccc=nx.connected_components(g)
	liste_cc=[]
	for cc in ccc:
		liste_cc.append(cc)

	g.remove_edges_from(hm)
	hm.clear()
#	print(hm)
	for cc in liste_cc:
		pas_candid=[]
		candid=[]
		for u in cc:
			if g.nodes[u]['candidate']==False:
				pas_candid.append(u)
			else:
				candid.append(u)

		g.remove_nodes_from(cc)
		nx.add_path(g,candid)
#		print(candid)
		for e in g.edges():
			hm.add(e)

	g.add_edges_from(ht)
	return g, ht, hm

def main():
    print("file: ",sys.argv[1])
    start_time = time.time()
    h = readFile(sys.argv[1])
    print("Graphe d'origine avec",len(h),"sommets et",h.number_of_edges(),"arêtes.")
    set_hetero_edge(h)
    ht = set([(v1,v2) for (v1,v2) in h.edges() if h[v1][v2]["hetero"]])
    hm = set([(v1,v2) for (v1,v2) in h.edges() if not h[v1][v2]["hetero"]])
    g,ht,hm = reduce_graph(h,ht,hm)

    #remove_useless(g)
    if not g:
        print("Couleur unique.")
        print("On a donc un résultat avec 0 chapeaux")
        return 0
    #print("Nombre de composantes connexes dans le graphe réduit avec",len(red),"sommets:",nx.number_connected_components(red))
    red = greedy(g,ht,hm)
    res = [n for n in red.nodes() if red.nodes[n]["chapeau"]]

    print("On a donc un résultat avec",len(res),"chapeaux qui sont:",res)
    print("--- %s seconds ---" % (time.time() - start_time))
    #print(check_result(g,red))


    #reduction.tulip_file(red,"out.tlp")





if __name__ == "__main__":
    main()