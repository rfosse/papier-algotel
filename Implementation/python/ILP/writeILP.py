import sys
import networkx as nx 

constraint = 0
def readFile(filename):
    g = nx.Graph()
    with open(filename) as f:
        for line in f:
            data = [str(x) for x in line.split()] 
            if data[0] == "v":
                g.add_node(int(data[1]),type=int(data[2]),candidate=False,chapeau=False)
            if data[0] == "e":
                g.add_edge(int(data[1]),int(data[2]),hetero=False)

    return g


def set_hetero_edge(g):
    for v1,v2 in g.edges():
        if g.nodes[v1]["type"] != g.nodes[v2]["type"]:
            g[v1][v2]["hetero"] = True
            g.nodes[v1]["candidate"] = True
            g.nodes[v2]["candidate"] = True
        else:
            g[v1][v2]["hetero"] = False


def create_var_nodes(g):
    var_nodes = []
    for i in range(len(g)):
        var_nodes.append("x"+str(i))
    return var_nodes

def create_var_edges(g):
    edges = list()
    for e1,e2 in g.edges():
        edges.append((e1,e2))
        
    return edges

def create_f_variable(g):
    f_list = []
    for u in g.nodes():
        for v in g.nodes():
            for v2 in g[v]:
                f_list.append((u,v,v2))

    return f_list 

def create_y_variable(g):
    y_list = []
    for u in g.nodes():
        for v in g.nodes():
            if u > v:
                continue
            y_list.append((u,v))
    
    return y_list


def create_z_variable(g):
    z_list = []
    for u,v in g.edges():
        z_list.append((u,v))
    return z_list

def create_h_variable(g):
    h_list = list()
    for e1,e2 in g.edges():
        h_list.append((e1,e2))
    return h_list

def write_bounds_h(f,g,h_list):
    i_h = 0
    for e1,e2 in h_list:
        if g[e1][e2]["hetero"]:
            f.write("h"+str(i_h)+" = 1\n")
        else:
            f.write("h"+str(i_h)+" = 0\n")
        i_h+=1


def return_index_double(l,val):
    u,v = val
    if u > v:
        u,v = v,u
    return l.index((u,v))



def get_index_loop(l):
    indexes = []
    for i in range(len(l)):
        if l[i][0] == l[i][1]:
            indexes.append(i)
    return indexes

def close_neighbor(g,v):
    res = [v]
    for n in g[v]:
        res.append(n)
    return res


def create_constraint_1(f,nodes,z_list):
    global constraint
    for u,v in z_list:
        f.write("c"+str(constraint)+": z"+str(z_list.index((u,v)))+" - "+str(nodes[u-1])+" >= 0\n")
        constraint+=1
        f.write("c"+str(constraint)+": z"+str(z_list.index((u,v)))+" - "+str(nodes[v-1])+" >= 0\n")
        constraint += 1
        f.write("c"+str(constraint)+": z"+str(z_list.index((u,v)))+" - "+str(nodes[u-1])+" - "+str(nodes[v-1])+" <= 0\n")
        constraint+=1 
    return 0


def create_constraint_2(f,f_list,y_list,z_list):
    global constraint
    for (u,v,v2) in f_list:
        f.write("c"+str(constraint)+": f"+str(f_list.index((u,v,v2)))+" - y"+str(return_index_double(y_list,(u,v2)))+" <= 0\n")
        constraint+=1
        f.write("c"+str(constraint)+": f"+str(f_list.index((u,v,v2)))+" - z"+str(return_index_double(z_list,(v,v2)))+" <= 0\n")
        constraint+=1
        f.write("c"+str(constraint)+": f"+str(f_list.index((u,v,v2)))+" + one - y"+str(return_index_double(y_list,(u,v2)))+" - z"+str(return_index_double(z_list,(v,v2)))+" >= 0\n")
        constraint+=1


def create_constraint_3(f,g,f_list,h_list,y_list):
    global constraint
    for u in g.nodes():
        for v in g.nodes():
            
            if len(g[v]) == 1 and u in g[v]:
                continue
            f.write("c"+str(constraint)+": y"+str(return_index_double(y_list,(u,v))))

            for v2 in g[v]:
                
                f.write(" + ")
                if g[v][v2]["hetero"]: # si h_v,v2 vaut 1
                    f.write("f"+str(f_list.index((u,v,v2))))
                else:
                    f.write(" one ")

            f.write(" >= 2\n")

            constraint+=1




def writeLPfile(g,filename="outputILP.lp"):
    global constraint
    nodes = create_var_nodes(g)
    edges = create_var_edges(g)
    f_list = create_f_variable(g)
    y_list = create_y_variable(g)
    z_list = create_z_variable(g)
    h_list = create_h_variable(g)

    f = open(filename,"w")

    f.write("Minimize\n")
    f.write("obj: ")
    for i in range(len(nodes)-1):
        f.write(nodes[i]+" ")
        f.write("+ ")
    f.write(nodes[-1])
    f.write("\n")
    
    f.write("Subject To\n")
    # f.write("c"+str(constraint)+": ")    
    # for i in range(len(nodes)-1):
    #     f.write(nodes[i]+" ")
    #     f.write("+ ")
    # f.write(nodes[-1]+" >= 1 ")
    # f.write("\n")
    # constraint += 1

    create_constraint_1(f,nodes,edges)
    f.write("\n")
    create_constraint_2(f,f_list,y_list,z_list)
    f.write("\n")
    create_constraint_3(f,g,f_list,h_list,y_list)
    f.write("\n")

    
    f.write("BOUNDS\n")
    #write_bounds_h(f,g,h_list)
    loop = get_index_loop(y_list)
    for l in loop:
        f.write("y"+str(l)+" = 1\n")
    f.write("one = 1\n")
    for n in nodes:
        f.write(n+" <= 1\n ")

    for i in range(len(y_list)):
        f.write("y"+(str(i))+" = 1\n")

    for i in range(len(f_list)):
        f.write("f"+(str(i))+" <= 1\n")

    for i in range(len(z_list)):
        f.write("z"+(str(i))+" <= 1\n")


    f.write("Binaries\n")
    for n in nodes:
        f.write(n+" ")
    f.write("\n")
    for i in range(len(y_list)):
        f.write("y"+(str(i))+" ")
    f.write("\n")
    for i in range(len(f_list)):
        f.write("f"+(str(i))+" ")
    f.write("\n")
    for i in range(len(z_list)):
        f.write("z"+(str(i))+" ")
    f.write("\n")

    f.write("END\n")


def main():
    g = readFile(str(sys.argv[1]))
    output = str(sys.argv[2])
    set_hetero_edge(g)
    writeLPfile(g,output)

    return 0

if __name__ == "__main__":
    main()