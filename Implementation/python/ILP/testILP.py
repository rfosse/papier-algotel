import cplex
import sys
import time

def print_sol(cpx):
    for i,x in enumerate(cpx.solution.get_values()):
        print("Solution value of ",cpx.variables.get_names(i),"is",x)
    print(cpx.solution.get_objective_value())

def main():
    t = time.time()
    cpx = cplex.Cplex(sys.argv[1])
    cpt = 0
    cpx.solve()
    print("Solution statut is:",cpx.solution.status[cpx.solution.get_status()])
    for i,x in enumerate(cpx.solution.get_values()):
        
        name = cpx.variables.get_names(i)
        if name.startswith("X"):
            if x > 0:
                cpt +=1
            print("Solution value of ",name,"is",x)
    print("Solution trouvée avec ",str(cpt),"chapeaux")
    print("Temps résolution est de:",time.time()-t)
    #print_sol(cpx)

if __name__ == "__main__":
    main()
