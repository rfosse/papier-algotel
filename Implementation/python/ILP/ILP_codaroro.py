#import reduction
import networkx as nx
import heapq
import sys
import time
from pulp import *
import cplex

#arg[fichier graphe en entrée]
#arg[2] graphe en sorte

def readFile(filename):
    g = nx.Graph()
    with open(filename) as f:
        for line in f:
            data = [str(x) for x in line.split()] 
            if data[0] == "v":
                g.add_node(int(data[1]),type=int(data[2]),candidate=False,chapeau=False)
            if data[0] == "e":
                g.add_edge(int(data[1]),int(data[2]),hetero=False)
    f.close()	
    return g

def setHetHom(g):
	het=set()
	hom=set()
	for e in g.edges():
		#print(e)
		u=e[0]
		v=e[1]
		if h.nodes[u]['type']!=h.nodes[v]['type']:
			h.edges[e]['hetero']=True
			het.add(e)
			h.nodes[u]['candidate']=True
			h.nodes[v]['candidate']=True
		else:
			hom.add(e)
#		print(h.edges[e]['hetero'])	
	return g, het, hom

def reduce_graph(h,ht,hm):
	g=h.copy()
	g.remove_edges_from(ht)
	ccc=nx.connected_components(g)
	liste_cc=[]
	for cc in ccc:
		liste_cc.append(cc)

	g.remove_edges_from(hm)
	hm.clear()
#	print(hm)
	for cc in liste_cc:
		pas_candid=[]
		candid=[]
		for u in cc:
			if g.nodes[u]['candidate']==False:
				pas_candid.append(u)
			else:
				candid.append(u)

		g.remove_nodes_from(cc)
		nx.add_path(g,candid)
#		print(candid)
		for e in g.edges():
			hm.add(e)
	ccc=nx.connected_components(g)		
	liste_cc=[]
	for cc in ccc:
		liste_cc.append(list(cc))
	g.add_edges_from(ht)
	return g, ht, hm, liste_cc






t = time.time()
h=readFile(sys.argv[1])
h, Ht, Hm =setHetHom(h)

#l faut doubler les arêtes....
#for e in hh.edges():
#	if e[0]!=e[1] and hh.edges[e]['hetero']==True:
#		h.add_edge(e[1],e[0],hetero=True)
#	if e[0]!=e[1] and hh.edges[e]['hetero']==False:
#		h.add_edge(e[1],e[0],hetero=False)

#h, Ht, Hm =setHetHom(h)
#print(Ht)
#print(Hm)

h,Ht,Hm, liste_cc =reduce_graph(h,Ht,Hm)



candidates=h.nodes()
n=len(candidates)



#couples va contenir le numéro d'indice de y en fonction du u et de v, ici ce sont les couples de CC
c=0
couples=dict()
for ccf in liste_cc:
	for ccp in [ccp for ccp in liste_cc if ccp!=ccf]:
		couples[liste_cc.index(ccf),liste_cc.index(ccp)]=c
		c+=1

#aretes va contenir le numéro d'indice de z en fonction de l'arête e
c=0
aretes=dict()
for a in h.edges():
#	print((a[0],a[1]))
#	print((a[1],a[0]))
	aretes[(a[0],a[1])]=c
	aretes[(a[1],a[0])]=c
	c+=1


#couple (compo connexe, hauteur)
hauteur=dict()
c=0
for cc in liste_cc:
	for i in range(0,len(liste_cc)):
		hauteur[liste_cc.index(cc),i]=c
		c+=1



#triplets va contenir le numéro d'indice de f en fonction de u, v et les voisins de v
c=0
triplets=dict()
for ccf in liste_cc:
	for ccp in [ccp for ccp in liste_cc if ccf!=ccp]:
#	for v in candidates:
		for l in range(1, len(liste_cc)):
			triplets[liste_cc.index(ccf),liste_cc.index(ccp),l]=c
			c+=1

#creation des variables
var_x=LpVariable.dicts("X",candidates,cat='Binary')
var_p=LpVariable.dicts("P",([e for e in couples.values()]),cat='Binary')
var_s=LpVariable.dicts("S",([e for e in couples.values()]),cat='Binary')
var_z=LpVariable.dicts("Z",([e for e in aretes.values()]),cat='Binary')
var_n=LpVariable.dicts("N",([e for e in hauteur.values()]),cat='Binary')
var_t=LpVariable.dicts("T",([e for e in triplets.values()]),cat='Binary')
#var_f=LpVariable.dicts("F",([t for t in triplets.values()]),cat='Binary')
	


prob=LpProblem("myProblem",LpMinimize)

#fonction objectif
prob+=lpSum(var_x[i] for i in var_x)

#contraintes sur les Z
for e in h.edges():
#	print(e)
	if e in Hm:
		prob+=var_z[aretes[e]]==1
	else:	
		prob+=var_z[aretes[e]]>=var_x[e[0]]
		prob+=var_z[aretes[e]]>=var_x[e[1]]
		prob+=var_z[aretes[e]]<=var_x[e[0]]+var_x[e[1]]

#contraintes sur les niveaux
#print(var_n)
for cc in liste_cc:
	l_cc=[]
	for l in range(0,len(liste_cc)):
#		print(var_n[hauteur[u,l]])
		l_cc.append(var_n[hauteur[liste_cc.index(cc),l]]) #chacun n'a qu'un seul niveau
	prob+=lpSum(l_cc)==1



prob+=var_n[hauteur[0,0]]==1 			#le sommets (CC) 0 est de niveau 0

#contraintes sur les pères
for ccf in liste_cc:	
	l_p=[]
	for ccp in [ccp for ccp in liste_cc if ccf!=ccp]:
		l_p.append(var_p[couples[liste_cc.index(ccf),liste_cc.index(ccp)]])
	if ccf==liste_cc[0]:
		prob+=lpSum(l_p)==0   # 0 n'a pas de père
	else:
		prob+=lpSum(l_p)==1  # les autres ont un seul père
	
for cc1 in liste_cc:
	for cc2 in [cc2 for cc2 in liste_cc if cc2!=cc1]:
		liste_c1c2=[]
		for u in cc1:
			for e in [e for e in h.edges(u) if ((e[0] in cc2) or (e[1] in cc2))]:
				prob+=var_s[couples[liste_cc.index(cc1),liste_cc.index(cc2)]]>=var_z[aretes[e]]
				liste_c1c2.append(var_z[aretes[e]])
		if len(liste_c1c2)>0:
			prob+=lpSum(liste_c1c2)>=var_s[couples[liste_cc.index(cc1),liste_cc.index(cc2)]]
		else:
			prob+=var_s[couples[liste_cc.index(cc1),liste_cc.index(cc2)]]==0
		
		



#contraintes sur les triplets, i.e., existe un i et un v tel que u niveau i, v niveau i-1 et z_uv couverte
for ccf in [ccf for ccf in liste_cc if ccf!=liste_cc[0]]:
	liste_ccf=[]
	for ccp in [ccp for ccp in liste_cc if ccp!=ccf]:
#	for v in candidates:
		for l in range(1,len(liste_cc)):
			prob+=var_t[triplets[liste_cc.index(ccf),liste_cc.index(ccp),l]]<=var_n[hauteur[liste_cc.index(ccf),l]]
			prob+=var_t[triplets[liste_cc.index(ccf),liste_cc.index(ccp),l]]<=var_n[hauteur[liste_cc.index(ccp),l-1]]
			prob+=var_t[triplets[liste_cc.index(ccf),liste_cc.index(ccp),l]]<=var_s[couples[(liste_cc.index(ccf),liste_cc.index(ccp))]]
			prob+=var_t[triplets[liste_cc.index(ccf),liste_cc.index(ccp),l]]+2>=var_n[hauteur[liste_cc.index(ccf),l]]+var_n[hauteur[liste_cc.index(ccp),l-1]]+var_s[couples[(liste_cc.index(ccf),liste_cc.index(ccp))]]
			liste_ccf.append(var_t[triplets[liste_cc.index(ccf),liste_cc.index(ccp),l]])
				
	prob+=lpSum(liste_ccf)>=1
	




# contraintes sur les Y
#for u in candidates:
#prob+=var_y[couples[0,0]]==1


#for u in candidates:
#	for v in [v for v in candidates if u!=v]:
#	for v in candidates:
#		cons=[var_y[couples[u,v]]]
#		for w in h.neighbors(v):
#			cons.append(var_f[triplets[u,v,w]])
#		prob+=lpSum(cons)>=2






prob.writeLP(sys.argv[2], writeSOS=1, mip=1)
print("Temps ecriture est de :",time.time()-t)
#status = prob.solve()

#print(status, value(prob.objective))

#for t in [t for t in triplets.keys()]:
#	if value(var_f[triplets[t]])!=0:
#		print ('triplet =', t, 'variable =F', triplets[t], 'valeur=', value(var_f[triplets[t]]), 'valeur couple=', t[1], t[2])

#print(var_y.values())#
#print([value(var_y[i]) for i in var_y])
#print(var_z.values())
#print([value(var_z[i]) for i in var_z])
#print(var_f.values())
#print([value(var_f[i]) for i in var_f])


