#import reduction
import networkx as nx
import heapq
import sys
import time
from pulp import *
import cplex

def readFile(filename):
    g = nx.Graph()
    with open(filename) as f:
        for line in f:
            data = [str(x) for x in line.split()] 
            if data[0] == "v":
                g.add_node(int(data[1]),type=int(data[2]),candidate=False,chapeau=False)
            if data[0] == "e":
                g.add_edge(int(data[1]),int(data[2]),hetero=False)
    f.close()	
    return g

def setHetHom(g):
	het=set()
	hom=set()
	for e in g.edges():
		#print(e)
		u=e[0]
		v=e[1]
		if h.nodes[u]['type']!=h.nodes[v]['type']:
			h.edges[e]['hetero']=True
			het.add(e)
			h.nodes[u]['candidate']=True
			h.nodes[v]['candidate']=True
		else:
			hom.add(e)
#		print(h.edges[e]['hetero'])	
	return g, het, hom

def reduce_graph(h,ht,hm):
	g=h.copy()
	g.remove_edges_from(ht)
	ccc=nx.connected_components(g)
	liste_cc=[]
	for cc in ccc:
		liste_cc.append(cc)

	g.remove_edges_from(hm)
	hm.clear()
#	print(hm)
	for cc in liste_cc:
		pas_candid=[]
		candid=[]
		for u in cc:
			if g.nodes[u]['candidate']==False:
				pas_candid.append(u)
			else:
				candid.append(u)

		g.remove_nodes_from(cc)
		nx.add_path(g,candid)
#		print(candid)
		for e in g.edges():
			hm.add(e)
	ccc=nx.connected_components(g)		
	liste_cc=[]
	for cc in ccc:
		liste_cc.append(list(cc))
	g.add_edges_from(ht)
	return g, ht, hm, liste_cc


def recompute_cc(g, ht, hm):
	g.remove_edges_from(ht)
	ccc=nx.connected_components(g)
	liste_cc=[]
	for cc in ccc:
		liste_cc.append(cc)
	g.add_edges_from(ht)
	return 	liste_cc

def find_concom(g,u, liste_cc):
	for l in liste_cc:
		if u in l:
			return l

def compute_super_degree(g,ht,hm, l_cc):
	max_deg=0
	max_u=list(g.nodes())[0]
	for u in g.nodes():
		liste_voisin=[]
		for v in g.neighbors(u):
#			if find_concom(g,u, l_cc)!=find_concom(g,v,l_cc):
			if (u,v) in Ht or (v,u) in Ht:
				liste_voisin.append(v)
		i=len(liste_voisin)
		if i>max_deg:
			max_deg=i
			max_u=u
#	print("max deg",max_deg, "max u",max_u)
	return max_u
	



t=time.time()
h=readFile(sys.argv[1])
h, Ht, Hm =setHetHom(h)

print("avant noeuds ",len(h.nodes()), " arêtes ",len(h.edges))
#print("noeuds : ",h.nodes())
#print("aretes : ", h.edges())
#print("heteros ",Ht)
#print("homo ", Hm)
h,Ht,Hm, liste_cc =reduce_graph(h,Ht,Hm)
print("avant noeuds ",len(h.nodes()), " arêtes ",len(h.edges))
#print("noeuds : ",h.nodes())
#print("aretes : ", h.edges())
#print("heteros ",Ht)
#"print("homo ", Hm)


#candidates=h.nodes()
#n=len(candidates)

chapeaux=[]
nb_chap=0
while len(liste_cc)>1:
	nb_chap+=1
	u=compute_super_degree(h, Ht, Hm, liste_cc)
	chapeaux.append(u)
#	print("voisinage de u",h.edges(u))
	activated=[]
#	print(Ht)
	for e in h.edges(u):
		u,v=e
		if (u,v) in Ht:
			activated.append((u,v))
		elif (v,u) in Ht:
			activated.append((v,u))
	for e in activated:	
		Ht.remove(e)
		Hm.add(e)
	liste_cc=recompute_cc(h, Ht, Hm)
#	print(len(liste_cc))

print("nombre de chapeaux", nb_chap)
print("temps = ",time.time()-t)
#print(chapeaux)
	
	



