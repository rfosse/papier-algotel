import sys
import glob
import os
import numpy as np


sommets = set()
def readfile(filename):
    global sommets
    seconds = []
    lecture = []
    ecriture = []
    chapeaux = []
    aretes = []
    with open(filename) as f:
        for line in f:
            data = [str(x) for x in line.split()]
            if "de:" in data:
                lecture.append(float(data[4]))
            if "ecriture" in data:
                ecriture.append(float(data[5]))
            if "chapeaux" in data:
                chapeaux.append(int(data[3]))
    seconds = [ lecture[i] + ecriture[i] for i in range(len(ecriture))]
    return lecture,ecriture,seconds,chapeaux

def complete_l(l):
    for ll in l:
        while(len(ll) < 40):
            ll.append(-1)
            
def mean_ll(l):
    res = []
    median = []
    for i in range(len(l[0])):
        m = 0
        cpt = 0
        tmp = []
        for ll in l:
            if ll[i] != -1:
                m += ll[i]
                tmp.append(ll[i])
                cpt += 1
        res.append(float(m)/max(cpt,1))
        median.append(np.median(tmp))
    return res,median



def main():
    dir = sys.argv[1]
    list_sec_total = []
    list_chapeaux_total = []
    list_ecriture_total = []
    list_lecture_total = []

    for file in os.listdir(dir):
        l,e,s,c = readfile(dir+"/"+file)
        list_ecriture_total.append(e)
        list_lecture_total.append(l)
        list_sec_total.append(s)
        list_chapeaux_total.append(c)

    l = []
    complete_l(list_sec_total)
    complete_l(list_chapeaux_total)
    complete_l(list_ecriture_total)
    complete_l(list_lecture_total)

    m_e,median_e = mean_ll(list_ecriture_total)
    m_l,median_l = mean_ll(list_lecture_total)
    m_c,median_c = mean_ll(list_chapeaux_total)
    m_s,median_s = mean_ll(list_sec_total)


    print("moyenne des chapeaux par taille:")
    for i in m_c:
        print(i)


    print("moyenne des secondes par taille:")
    for i in m_s:
        print(i)

    print("median des secondes par taille:")
    for i in median_s:
        print(i)

    print("moyenne ecriture par taille:")
    for i in m_e:
        print(i)
    
    print("mediane ecriture par taille:")
    for i in median_e:
        print(i)
    
    print("moyenne lecture par taille:")
    for i in m_l:
        print(i)
    
    print("mediane lecture par taille:")
    for i in median_l:
        print(i)

    return 0

if __name__ == "__main__":
    main()