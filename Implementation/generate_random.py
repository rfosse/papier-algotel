import networkx as nx
import sys
import random
import math
import subprocess


def Erdos(n,p):
    seed = random.randint(0,1000000)
    print("le seed du graphe est",seed)
    return nx.fast_gnp_random_graph(n,p,seed)

def uniq_color(c):
    v = c.values()
    return len(set(v)) == 1
def random_coloration(g):
    c = dict()
    for i in range(len(g.nodes())):
        c[i] = random.randint(0, 1)
    
    if uniq_color(c):
        random_coloration(g)
    nx.set_node_attributes(g,c,"color")

def generate_output(g,filename):
    with open(filename,"w") as f:
        for n in g.nodes():
            f.write("v "+str(n)+" "+str(g.nodes[n]["color"])+"\n")
        for v1,v2 in g.edges():
            f.write("e "+str(v1)+" "+str(v2)+"\n")

def barabasi(n,m):
    seed = random.randint(0,1000000)
    print("le seed du graphe est",seed)
    return nx.barabasi_albert_graph(n,m,seed)

def gengraph(n):
    g = nx.Graph()
    with open("gengraph.out","w") as f:
        cmd = "planar "+str(n)+" 3 3"
        print(cmd)
        subprocess.call(["./gengraph","planar",str(n),"3","3","-format","list"],stdout=f)

    with open("gengraph.out") as f:
        for line in f:
            data = [str(x) for x in line.split()]
            for i in range(1,len(data)): 
                g.add_edge(int(data[0][:-1]),int(data[i]))

    return g

def catterpillar(n):
    g = nx.Graph()

    for i in range(n-1):
        print(i,i+1)
        g.add_edge(i,i+1)
        g.add_edge(i,n+i)
    g.add_edge(n-1,2*n-1)
    color = dict()
    for i in range(n):
        color[i] = 0
    for i in range(n,g.number_of_nodes()):
        color[i] = 1

    nx.set_node_attributes(g,color, "color")

    return g

def main():
    type = str(sys.argv[1])
    i = 2
    g = nx.Graph()
    if type == "e":
        n = int(sys.argv[i])
        p = (math.log(n) + 5)/n
        while True:
            g = Erdos(n,p)
            if nx.is_connected(g):
                break
        i += 1
    elif type == "b":
        n = int(sys.argv[i])
        i += 1
        m = int(sys.argv[i])
        i+=1
        g = barabasi(n,m)
    elif type == "g":
        n = int(sys.argv[i])
        i += 1
        g = gengraph(n)
    elif type == "c":
        n = int(sys.argv[i])
        i += 1
        g = catterpillar(n)
    if type != "c":
        random_coloration(g)
    filename = str(sys.argv[i])
    generate_output(g,str(filename))
    






if __name__ == "__main__":
    main()



